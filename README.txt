Window placer tool written in C for Microsoft Windows by Jens Åkerblom.
No warranty but tested on Windows 10.

- License -
Public domain.

- Usage -
While program is running, hold windows key and press numpad keys to move and resize active window. Numpad 2, 4, 6 and 8 moves the window to cover half of the screen space, numpad 1, 3, 7 and 9 only the corner regions. Use numpad / * and - to subdivide into thirds of the window, two thirds if you hold left shift as well.

- Build instructions -
Start Visual Studio command line tools (tested on VS2017 x64).
Run build.bat.