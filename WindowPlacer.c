#include <Windows.h>
#include <shellapi.h>

#pragma comment (lib, "User32.lib")
#pragma comment (lib, "Shell32.lib")

#define StaticAssert(cnd)

#define DWMWA_EXTENDED_FRAME_BOUNDS (DWORD)9
typedef HRESULT (__stdcall *DwmGetWindowAttributeFunc)(HWND, DWORD, PVOID, DWORD);
static DwmGetWindowAttributeFunc DwmGetWindowAttribute = NULL;

typedef enum
{
	Direction_None,

	Direction_Right,
	Direction_Up,
	Direction_Left,
	Direction_Down,
} Direction;

typedef struct
{
	unsigned int count;
	HMONITOR e[32];
} Monitors;

static BOOL __stdcall MonitorEnum(HMONITOR monitor, HDC hdc, LPRECT rect, LPARAM lParam)
{
	Monitors* monitors = (Monitors*)lParam;

	if (monitors->count < sizeof(monitors->e) / sizeof(*monitors->e))
	{
		monitors->e[monitors->count++] = monitor;
	}

	return TRUE;
}

static Monitors EnumerateMonitors(void)
{
	Monitors rVal = { 0 };
	EnumDisplayMonitors(NULL, NULL, MonitorEnum, (LPARAM)&rVal);

	return rVal;
}

static HMONITOR FindMonitorInDirection(Monitors* monitors, HMONITOR monitor, Direction dir)
{
	HMONITOR rVal = 0;

	MONITORINFO info = { sizeof(info) };
	if (GetMonitorInfoA(monitor, &info))
	{
		RECT sourceRect = info.rcMonitor;

		unsigned int distanceToMonitor = -1;

		for (HMONITOR* testMonitor = monitors->e; testMonitor != monitors->e + monitors->count; ++testMonitor)
		{
			if (*testMonitor != monitor)
			{
				MONITORINFO testInfo = { sizeof(testInfo) };
				if (GetMonitorInfoA(*testMonitor, &testInfo))
				{
					RECT testRect = testInfo.rcMonitor;

					int overlapMinX = (sourceRect.left > testRect.left ? sourceRect.left : testRect.left);
					int overlapMaxX = (sourceRect.right < testRect.right ? sourceRect.right : testRect.right);

					int overlapMinY = (sourceRect.top > testRect.top ? sourceRect.top : testRect.top);
					int overlapMaxY = (sourceRect.bottom < testRect.bottom ? sourceRect.bottom : testRect.bottom);

					unsigned int distance = -1;

					// NOTE(Jens): This assumes monitors don't overlap.

					if (overlapMinX < overlapMaxX)
					{
						if (sourceRect.top >= testRect.bottom)
						{
							if (dir == Direction_Up)
							{
								distance = (unsigned int)(sourceRect.top - testRect.bottom);
							}
						}
						else if (sourceRect.bottom <= testRect.top)
						{
							if (dir == Direction_Down)
							{
								distance = (unsigned int)(testRect.top - sourceRect.bottom);
							}
						}
					}
					else if (overlapMinY < overlapMaxY)
					{
						if (sourceRect.left >= testRect.right)
						{
							if (dir == Direction_Left)
							{
								distance = (unsigned int)(testRect.right - sourceRect.left);
							}
						}
						else if (sourceRect.right <= testRect.left)
						{
							if (dir == Direction_Right)
							{
								distance = (unsigned int)(testRect.left - sourceRect.right);
							}
						}
					}
					else
					{
						// NOTE(Jens): Disjoint monitors, can we move to it at all? Maybe check axis distance and take the smallest difference.
					}

					if (distance < distanceToMonitor)
					{
						rVal = *testMonitor;
						distanceToMonitor = distance;
					}
				}
			}
		}
	}

	return rVal;
}

typedef unsigned int KeyFlag;
enum
{
	KeyFlag_LeftWin   = (1 <<  0),
	KeyFlag_LeftCtrl  = (1 <<  1),
	KeyFlag_Numpad0   = (1 <<  2),
	KeyFlag_Numpad1   = (1 <<  3),
	KeyFlag_Numpad2   = (1 <<  4),
	KeyFlag_Numpad3   = (1 <<  5),
	KeyFlag_Numpad4   = (1 <<  6),
	KeyFlag_Numpad5   = (1 <<  7),
	KeyFlag_Numpad6   = (1 <<  8),
	KeyFlag_Numpad7   = (1 <<  9),
	KeyFlag_Numpad8   = (1 << 10),
	KeyFlag_Numpad9   = (1 << 11),
	KeyFlag_NumpadDiv = (1 << 12),
	KeyFlag_NumpadMul = (1 << 13),
	KeyFlag_NumpadSub = (1 << 14),
	KeyFlag_Shift     = (1 << 15),
};

static inline KeyFlag VKKeyToFlag(DWORD vkCode)
{
	KeyFlag rVal = 0;

	switch (vkCode)
	{
		case VK_LWIN:
		{
			rVal = KeyFlag_LeftWin;
		} break;
		
		case VK_LCONTROL:
		{
			rVal = KeyFlag_LeftCtrl;
		} break;

		case VK_LSHIFT:
		{
			rVal = KeyFlag_Shift;
		} break;
		
		case VK_NUMPAD0:
		{
			rVal = KeyFlag_Numpad0;
		} break;
		
		case VK_NUMPAD1:
		{
			rVal = KeyFlag_Numpad1;
		} break;

		case VK_NUMPAD2:
		{
			rVal = KeyFlag_Numpad2;
		} break;

		case VK_NUMPAD3:
		{
			rVal = KeyFlag_Numpad3;
		} break;

		case VK_NUMPAD4:
		{
			rVal = KeyFlag_Numpad4;
		} break;

		case VK_NUMPAD5:
		{
			rVal = KeyFlag_Numpad5;
		} break;

		case VK_NUMPAD6:
		{
			rVal = KeyFlag_Numpad6;
		} break;

		case VK_NUMPAD7:
		{
			rVal = KeyFlag_Numpad7;
		} break;

		case VK_NUMPAD8:
		{
			rVal = KeyFlag_Numpad8;
		} break;

		case VK_NUMPAD9:
		{
			rVal = KeyFlag_Numpad9;
		} break;
		
		case VK_MULTIPLY:
		{
			rVal = KeyFlag_NumpadMul;
		} break;
		
		case VK_DIVIDE:
		{
			rVal = KeyFlag_NumpadDiv;
		} break;
		
		case VK_SUBTRACT:
		{
			rVal = KeyFlag_NumpadSub;
		} break;
	}

	return rVal;
}

typedef struct 
{
	unsigned int gridW;
	unsigned int gridH;

	unsigned int x;
	unsigned int y;

	unsigned int w;
	unsigned int h;
} Location;

static RECT GetRectFromLocation(Location location, RECT area)
{
	// TODO(Jens): Handle division truncation.

	RECT rVal = { 0 };

	LONG cellWidth = (area.right - area.left) / location.gridW;
	LONG cellHeight = (area.bottom - area.top) / location.gridH;

	rVal.left = cellWidth * location.x + area.left;
	rVal.top = cellHeight * location.y + area.top;

	rVal.right = rVal.left + cellWidth * location.w;
	rVal.bottom = rVal.top + cellHeight * location.h;

	return rVal;
}

static int RectsAreEqual(RECT a, RECT b)
{
	int rVal = (a.left == b.left && a.right == b.right && a.top == b.top && a.bottom == b.bottom);
	return rVal;
}

static int SnapFocusedWindow(KeyFlag keysPressed, KeyFlag key)
{
	int rVal = 0;
	if (keysPressed & KeyFlag_LeftWin)
	{
		HWND windowToMove = GetForegroundWindow();
		if (windowToMove)
		{
			HMONITOR monitor = MonitorFromWindow(windowToMove, MONITOR_DEFAULTTONEAREST);
			if (monitor) // NOTE(Jens): This should probably never be NULL but might as well play it safe.
			{
				MONITORINFO info = { sizeof(info) };
				if (GetMonitorInfoA(monitor, &info))
				{
					RECT workArea = info.rcWork;

					int workW = (int)(workArea.right - workArea.left);
					int workH = (int)(workArea.bottom - workArea.top);

					int maximize = 0;
					Direction swapDirection = Direction_None;

					int shiftDown = (keysPressed & KeyFlag_Shift);
					Location location = { 0 };
					
					switch (key)
					{
						case KeyFlag_NumpadDiv:
						{
							location.x = 0;
							location.y = 0;
							location.w = 1;
							location.h = 1;

							if (workW >= workH)
							{
								location.gridW = 3;
								location.gridH = 1;

								if (shiftDown)
								{
									++location.w;
								}
							}
							else
							{
								location.gridW = 1;
								location.gridH = 3;

								if (shiftDown)
								{
									++location.h;
								}
							}
						} break;
						
						case KeyFlag_NumpadMul:
						{
							location.w = 1;
							location.h = 1;

							if (workW >= workH)
							{
								location.x = 1;
								location.y = 0;

								location.gridW = 3;
								location.gridH = 1;
							}
							else
							{
								location.x = 0;
								location.y = 1;

								location.gridW = 1;
								location.gridH = 3;
							}
						} break;
						
						case KeyFlag_NumpadSub:
						{
							location.w = 1;
							location.h = 1;

							if (workW >= workH)
							{
								location.gridW = 3;
								location.gridH = 1;
								
								location.x = 2;
								location.y = 0;

								if (shiftDown)
								{
									++location.w;
									--location.x;
								}
							}
							else
							{
								location.gridW = 1;
								location.gridH = 3;

								location.x = 0;
								location.y = 2;

								if (shiftDown)
								{
									++location.h;
									--location.y;
								}
							}
						} break;
						
						case KeyFlag_Numpad1:
						{
							location.gridW = 2;
							location.gridH = 2;
							location.x = 0;
							location.y = 1;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad2:
						{
							swapDirection = Direction_Down;

							location.gridW = 1;
							location.gridH = 2;
							location.x = 0;
							location.y = 1;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad3:
						{
							location.gridW = 2;
							location.gridH = 2;
							location.x = 1;
							location.y = 1;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad4:
						{
							swapDirection = Direction_Left;

							location.gridW = 2;
							location.gridH = 1;
							location.x = 0;
							location.y = 0;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad5:
						{
							maximize = 1;
						} break;

						case KeyFlag_Numpad6:
						{
							swapDirection = Direction_Right;

							location.gridW = 2;
							location.gridH = 1;
							location.x = 1;
							location.y = 0;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad7:
						{
							location.gridW = 2;
							location.gridH = 2;
							location.x = 0;
							location.y = 0;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad8:
						{
							swapDirection = Direction_Up;

							location.gridW = 1;
							location.gridH = 2;
							location.x = 0;
							location.y = 0;
							location.w = 1;
							location.h = 1;
						} break;

						case KeyFlag_Numpad9:
						{
							location.gridW = 2;
							location.gridH = 2;
							location.x = 1;
							location.y = 0;
							location.w = 1;
							location.h = 1;
						} break;
					}

					if (maximize)
					{
						ShowWindow(windowToMove, SW_MAXIMIZE);
						rVal = 1;
					}
					else if (location.w && location.h)
					{
						RECT newWinRect = GetRectFromLocation(location, workArea);

						ShowWindow(windowToMove, SW_SHOWNORMAL);

						{
							RECT winRect = { 0 };
							RECT dwmRect;

							int leftPadding = 0;
							int rightPadding = 0;
							int topPadding = 0;
							int bottomPadding = 0;

							if (GetWindowRect(windowToMove, &winRect) && DwmGetWindowAttribute && DwmGetWindowAttribute(windowToMove, DWMWA_EXTENDED_FRAME_BOUNDS, &dwmRect, sizeof(dwmRect)) == S_OK)
							{
								leftPadding = winRect.left - dwmRect.left;
								topPadding = winRect.top - dwmRect.top;
								rightPadding = winRect.right - dwmRect.right;
								bottomPadding = winRect.bottom - dwmRect.bottom;
							}

							newWinRect.left += leftPadding;
							newWinRect.right += rightPadding;
							newWinRect.top += topPadding;
							newWinRect.bottom += bottomPadding;

							if (RectsAreEqual(winRect, newWinRect) && swapDirection)
							{
								// NOTE(Jens): Try to move to other monitor.

								Monitors monitors = EnumerateMonitors();
								HMONITOR adjacentMonitor = FindMonitorInDirection(&monitors, monitor, swapDirection);
								if (adjacentMonitor)
								{
									MONITORINFO adjacentInfo = { sizeof(adjacentInfo) };
									if (GetMonitorInfoA(adjacentMonitor, &adjacentInfo))
									{
										newWinRect = GetRectFromLocation(location, adjacentInfo.rcWork);

										newWinRect.left += leftPadding;
										newWinRect.right += rightPadding;
										newWinRect.top += topPadding;
										newWinRect.bottom += bottomPadding;
									}
								}
							}
						}

						// NOTE(Jens): Windows is odd...
						SetWindowPos(windowToMove, HWND_TOP, newWinRect.left, newWinRect.top, newWinRect.right - newWinRect.left, newWinRect.bottom - newWinRect.top, SWP_NOSIZE);
						SetWindowPos(windowToMove, HWND_TOP, newWinRect.left, newWinRect.top, newWinRect.right - newWinRect.left, newWinRect.bottom - newWinRect.top, SWP_NOMOVE);

						rVal = 1;
					}
				}
			}
		}
	}

	return rVal;
}

static KeyFlag KeysPressed = 0;

static LRESULT __stdcall KeyboardHookLowLevel(int nCode, WPARAM wParam, LPARAM lParam)
{
	int rVal = 0;

	if (nCode >= 0)
	{
		KBDLLHOOKSTRUCT* data = (KBDLLHOOKSTRUCT*)lParam;

		KeyFlag flag = VKKeyToFlag(data->vkCode);

		switch (wParam)
		{
			case WM_SYSKEYDOWN:
			case WM_KEYDOWN:
			{
				if (flag)
				{
					KeyFlag wasDown = (KeysPressed & flag);
					if (!wasDown)
					{
						rVal = SnapFocusedWindow(KeysPressed, flag);
						KeysPressed |= flag;
					}
				}
			} break;

			case WM_SYSKEYUP:
			case WM_KEYUP:
			{
				KeysPressed &= ~flag;
			} break;
		}
	}

	return rVal ? rVal : CallNextHookEx(NULL, nCode, wParam, lParam);
}

typedef enum PROCESS_DPI_AWARENESS {
	PROCESS_DPI_UNAWARE = 0,
	PROCESS_SYSTEM_DPI_AWARE = 1,
	PROCESS_PER_MONITOR_DPI_AWARE = 2
} PROCESS_DPI_AWARENESS;
typedef HRESULT(__stdcall *SetProcessDpiAwarenessFunc)(PROCESS_DPI_AWARENESS);

static void ShowPopup(int isError, const char* title, const char* text)
{
	MessageBoxExA(NULL, text, title, MB_OK | (isError ? MB_ICONERROR : 0), MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US));
}

#define WPNotificationMessage (WM_USER + 1)

LRESULT __stdcall WindowProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT rVal = 0;

	switch (message)
	{
		case WPNotificationMessage:
		{
			switch (lParam)
			{
				case WM_LBUTTONDOWN:
				case WM_RBUTTONDOWN:
				{
					POINT mousePos = { 0 };
					GetCursorPos(&mousePos);

					unsigned int counter = 0;

					HMENU menu = CreatePopupMenu();
					
					MENUITEMINFOA exitItem = { sizeof(exitItem) };
					exitItem.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID;
					exitItem.fType = MFT_STRING;
					exitItem.dwTypeData = "Quit Window Placer";
					exitItem.fState = MFS_DEFAULT | MFS_ENABLED;
					exitItem.wID = ++counter;

					InsertMenuItem(menu, 0, TRUE, &exitItem);

					MENUITEMINFOA helpItem = { sizeof(helpItem) };
					helpItem.fMask = MIIM_TYPE | MIIM_STATE | MIIM_ID;
					helpItem.fType = MFT_STRING;
					helpItem.dwTypeData = "Help";
					helpItem.fState = MFS_ENABLED;
					helpItem.wID = ++counter;

					InsertMenuItem(menu, 0, TRUE, &helpItem);

					SetForegroundWindow(window);

					BOOL id = TrackPopupMenu(
						menu,
						TPM_CENTERALIGN | TPM_VCENTERALIGN | TPM_RETURNCMD | TPM_LEFTBUTTON,
						mousePos.x,
						mousePos.y,
						0,
						window,
						0
					);

					if (id == exitItem.wID)
					{
						PostQuitMessage(0);
					}
					else if (id == helpItem.wID)
					{
						ShowPopup(0, "How to use...", "Use Win Key + Numpad # to move window to screen area.");
					}

					DestroyMenu(menu);
				} break;

				default:
				{
					
				} break;
			}
		} break;

		default:
		{
			rVal = DefWindowProcA(window, message, wParam, lParam);
		} break;
	}

	return rVal;
}

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HANDLE applicationMutex = CreateMutexA(NULL, TRUE, "WindowPlacerMutex");
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		ShowPopup(1, "Already running", "Only one Window Placer may run at the same time.");
		
		// FIXME(Jens): Would be better to just quit the already running application, if user wants to open this he / she should be able to.
	}
	else
	{
		HWND window = 0;
		
		WNDCLASSA winClass = { 0 };
		winClass.style = CS_NOCLOSE;
		winClass.lpfnWndProc = WindowProc;
		winClass.hInstance = hInstance;
		winClass.lpszClassName = "WindowPlacerClass";

		if (RegisterClassA(&winClass))
		{
			window = CreateWindowExA(
				WS_EX_NOACTIVATE,
				winClass.lpszClassName,
				"Dummy - Should be invisible",
				WS_OVERLAPPED,
				0,
				0,
				0,
				0,
				0,
				0,
				hInstance, 
				0
			);

			if (window)
			{
				HHOOK keyboardHook = SetWindowsHookExA(WH_KEYBOARD_LL, KeyboardHookLowLevel, NULL, 0);
				if (keyboardHook)
				{
					NOTIFYICONDATAA notifyData = { sizeof(notifyData) };
					notifyData.hWnd = window;
					notifyData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
					notifyData.uCallbackMessage = WPNotificationMessage;
					notifyData.hIcon = LoadIcon(hInstance, "TrayIcon");

					char tip_[] = "Window Placer\nWinKey + Numpad # to move active window.";
					char* tip = tip_;

					StaticAssert(sizeof(tip) <= sizeof(notifyData.szTip));

					unsigned int counter = 0;

					while (*tip)
					{
						if (counter == sizeof(notifyData.szTip) - 1)
						{
							__debugbreak();
							break;
						}

						notifyData.szTip[counter++] = *tip++;
					}

					if (Shell_NotifyIconA(NIM_ADD, &notifyData))
					{
						// NOTE(Jens): Modules and handles get offloaded at process termination, however the notification icon does not. 
						//             Maybe add a structured exception guard to ensure it's removed even after crashes.

						{
							HMODULE dwmModule = LoadLibraryA("Dwmapi.dll");
							if (dwmModule)
							{
								DwmGetWindowAttribute = (DwmGetWindowAttributeFunc)GetProcAddress(dwmModule, "DwmGetWindowAttribute");
							}
						}

						{
							HMODULE shellScalingModule = LoadLibraryA("Shcore.dll");
							if (shellScalingModule)
							{
								// NOTE(Jens): Seems just telling the OS that this application is DPI aware let's us specify the coordinates without caring about the DPI.
								SetProcessDpiAwarenessFunc SetProcessDpiAwareness = (SetProcessDpiAwarenessFunc)GetProcAddress(shellScalingModule, "SetProcessDpiAwareness");
								if (SetProcessDpiAwareness)
								{
									SetProcessDpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
								}
							}
						}
	
						MSG msg = { 0 };
						while (GetMessageA(&msg, NULL, 0, 0) > 0)
						{
							TranslateMessage(&msg);
							DispatchMessage(&msg);
						}

						Shell_NotifyIconA(NIM_DELETE, &notifyData);
					}
					else
					{
						ShowPopup(1, "Error", "Failed to add notification icon (bottom right).");
					}
				}
				else
				{
					ShowPopup(1, "Error", "Failed to hook keyboard input.");
				}
			}
			else
			{
				ShowPopup(1, "Error", "Failed to create dummy window.");
			}
		}
		else
		{
			ShowPopup(1, "Error", "Failed to register window.");
		}
	}

	return 0;
}
