@echo off

REM Assumes you are running from developer command prompt for visual studio

rc /nologo /fo Resources.res Resources.rc

if "%~1"=="debug" (
	echo Building debug...
	cl WindowPlacer.c /nologo /Od /MTd /Z7 /FC /link /PDB:WindowPlacer.pdb /OUT:WindowPlacer.exe /INCREMENTAL:NO /OPT:REF Resources.res
) else (
	echo Building release...
	cl WindowPlacer.c /nologo /O2 /MT /link /OUT:WindowPlacer.exe /INCREMENTAL:NO /OPT:REF Resources.res
)
